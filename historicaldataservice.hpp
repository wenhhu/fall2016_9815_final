/**
 * historicaldataservice.hpp
 * historicaldataservice.hpp
 *
 * @author Breman Thuraisingham
 * Defines the data types and Service for historical data.
 *
 * @author Breman Thuraisingham
 */
#ifndef HISTORICAL_DATA_SERVICE_HPP
#define HISTORICAL_DATA_SERVICE_HPP

/**
 * Service for processing and persisting historical data to a persistent store.
 * Keyed on some persistent key.
 * Type T is the data type to persist.
 */

#include "common.hpp"

class ExecutionDataConnector : Connector<string> {
public:

    virtual void Publish(string &data) {
        ofstream os("executions.txt", ios_base::app);
        os << data << endl;
    }

    ExecutionDataConnector() {
    }
};

class RiskDataConnector : Connector<string> {
public:

    RiskDataConnector() {
    }

    virtual void Publish(string &data) {
        ofstream os("risk.txt", ios_base::app);
        os << data << endl;
        os.close();
    }
};

class StreamingDataConnector : Connector<string> {
public:

    virtual void Publish(string &data) {
        ofstream os("streaming.txt", ios_base::app);
        os << data << endl;
        os.close();
    }

    StreamingDataConnector() {
    }

};

class InquiryDataConnector : Connector<string> {
public:

    InquiryDataConnector() {
    }

    virtual void Publish(string &data) {
        ofstream os("allinquiries.txt", ios_base::app);
        os << data << endl;
    }

};

//this class is implemented with singleton paradigm since it will be called by many listeners

class HistoricalDataService : Service<string, Bond> {
public:

    static HistoricalDataService* get_instance() {
        static HistoricalDataService instance;
        return &instance;
    }

    void OnMessage(Bond& data) {
    }

    // Persist data to a store

    void PersistData(string persistKey, const Bond& data) {
    }
    void PersistExecutionData(string msg);
    void PersistRiskData(PV01<Bond> & msg);
    void PersistInquiryData(Inquiry<Bond>& msg);
    void PersistStreamingData(const PriceStream<Bond>&);
private:
    HistoricalDataService();
    ExecutionDataConnector *exe_connector;
    RiskDataConnector *risk_connector;
    StreamingDataConnector *streaming_connector;
    InquiryDataConnector *inquiry_connector;

};

class HistoricalDataServiceListener : public ServiceListener<OrderBook<Bond>>
{
    public:
    // update the orderbook
    void ProcessUpdate(OrderBook<Bond> &data);
    HistoricalDataServiceListener();

    private:
    HistoricalDataService * service;
};

void HistoricalDataService::PersistExecutionData(string msg) {
    exe_connector->Publish(msg);
}

void HistoricalDataService::PersistRiskData(PV01<Bond> & pv01) {
    string msg = "PV01 of current position is " + std::to_string(pv01.GetPV01());
    risk_connector->Publish(msg);
}

void HistoricalDataService::PersistInquiryData(Inquiry<Bond>& inq) {
    cout << "Persisting InquiryData\n";
    string msg;
    msg += inq.GetInquiryId();
    if (inq.GetSide() == Side::BUY)
        msg += " BUY ";
    else msg += " SELL ";
    msg += inq.GetProduct().GetProductId() + " ";
    msg += std::to_string(inq.GetQuantity()) + " ";
    msg += std::to_string(inq.GetPrice()) + "$";
    inquiry_connector->Publish(msg);
}

void HistoricalDataService::PersistStreamingData(const PriceStream<Bond>& data) {
    string msg = data.GetProduct().GetProductId() + "   $";
    msg += to_string(data.GetBidOrder().GetPrice()) + "  $";
    msg += to_string(data.GetOfferOrder().GetPrice());
    streaming_connector->Publish(msg);
}

//all four modules are assembled here for output
HistoricalDataService::HistoricalDataService() {
    exe_connector = new ExecutionDataConnector;
    risk_connector = new RiskDataConnector;
    inquiry_connector = new InquiryDataConnector;
    streaming_connector = new StreamingDataConnector;
}

#endif
