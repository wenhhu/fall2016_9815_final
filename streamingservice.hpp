/**
 * streamingservice.hpp
 * Defines the data types and Service for price streams.
 *
 * @author Breman Thuraisingham
 */
#ifndef STREAMING_SERVICE_HPP
#define STREAMING_SERVICE_HPP

#include "soa.hpp"
#include "marketdataservice.hpp"
#include "common.hpp"

/**
 * Streaming service to publish two-way prices.
 * Keyed on product identifier.
 * Type T is the product type.
 */

class StreamingService : public Service<string, PriceStream <Bond> > {
public:

    // Publish two-way prices
    void PublishPrice(const PriceStream<Bond>& priceStream);
    void OnMessage(PriceStream <Bond>&);

};


class StreamingListener : public ServiceListener<Price<Bond>>
{
    StreamingService* service;
    public:
    void ProcessAdd(Price<Bond>&);

    StreamingListener() {
        service = new StreamingService;
    }

};

void StreamingService::OnMessage(PriceStream<Bond>& data){
    cout << "StreamingService...\n";
    PublishPrice(data);
}

void StreamingService::PublishPrice(const PriceStream<Bond>& priceStream) {
    cout << "StreamingService...\n";
    HistoricalDataService::get_instance()->PersistStreamingData(priceStream);
}

//convert Price to PriceStream
void StreamingListener::ProcessAdd(Price<Bond>& data) {
    double mid = data.GetMid();
    double spread = data.GetBidOfferSpread();
    double offer = mid + spread / 2, bid = mid - spread / 2;
    // no size provided, therefore use 0...
    PriceStreamOrder bid_order(bid, 0, 0, PricingSide::BID);
    PriceStreamOrder offer_order(offer, 0, 0, PricingSide::OFFER);
    PriceStream<Bond> ps(data.GetProduct(), bid_order, offer_order);
    service->PublishPrice(ps);
}

#endif
