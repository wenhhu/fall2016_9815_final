/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Trade.hpp
 * Author: wenhaohu
 *
 * Created on December 17, 2016, 11:32 PM
 */

#ifndef TRADE_HPP
#define TRADE_HPP

#include <string>
#include <vector>
#include "soa.hpp"
#include "products.hpp"
#include "boost/date_time/gregorian/gregorian.hpp"

using namespace boost::gregorian;

//All the infrastructure function and class are putted in this file which makes them easier to be referenced.

// Trade sides

enum Side {
    BUY, SELL
};

// Various inqyury states

enum InquiryState {
    RECEIVED, QUOTED, DONE, REJECTED, CUSTOMER_REJECTED
};

//Global variables

string BOOKs[3] = {"TRSY1", "TRSY2", "TRSY3"};
string CUSIPs[6] = {"912828U40", "912828U73", "912828U65", "912828U57", "912828U24", "912810RU4"};
string MaturityDate[6] = {"2018/11/30", "2019/12/15", "2021/11/30", "2023/11/30", "2026/11/15", "2046/11/15"};
double Coupon[6] = {1.000, 1.375, 1.750, 2.125, 2.000, 2.875};
map<string, pair<double, date>> BondInfo = {
    {CUSIPs[0], make_pair(Coupon[0], date(2018, 11, 30))},
    {CUSIPs[1], make_pair(Coupon[1], date(2019, 12, 15))},
    {CUSIPs[2], make_pair(Coupon[2], date(2021, 11, 30))},
    {CUSIPs[3], make_pair(Coupon[3], date(2023, 11, 30))},
    {CUSIPs[4], make_pair(Coupon[4], date(2026, 11, 15))},
    {CUSIPs[5], make_pair(Coupon[5], date(2046, 11, 15))}};

// Side for market data

enum PricingSide {
    BID, OFFER
};

/**
 * A market data order with price, quantity, and side.
 */
class Order {
public:

    // ctor for an order
    Order(double _price, long _quantity, PricingSide _side);

    // Get the price on the order
    double GetPrice() const;

    // Get the quantity on the order
    long GetQuantity() const;

    // Get the side on the order
    PricingSide GetSide() const;

private:
    double price;
    long quantity;
    PricingSide side;

};

/**
 * Order book with a bid and offer stack.
 * Type T is the product type.
 */
template<typename T>
class OrderBook {
public:

    // ctor for the order book
    OrderBook(const T &_product, const vector<Order> &_bidStack, const vector<Order> &_offerStack);

    // Get the product
    const T& GetProduct() const;

    // Get the bid stack
    const vector<Order>& GetBidStack() const;

    // Get the offer stack
    const vector<Order>& GetOfferStack() const;

private:
    T product;
    vector<Order> bidStack;
    vector<Order> offerStack;

};

template<typename T>
class Position {
public:

    // ctor for a position
    Position(const T &_product);

    Position();

    // Get the product
    const T& GetProduct() const;

    // Get the position quantity
    long GetPosition(string &book);

    // Get the aggregate position
    long GetAggregatePosition() const;

    // Add share to a certain book
    void AddPosition(string& book, int n);
    void AddPosition(Position<T>&);

private:
    T product;
    map<string, long> positions;

};

template<typename T>
class Trade {
public:

    // ctor for a trade
    Trade(const T &_product, string _tradeId, string _book, long _quantity, Side _side);

    // Get the product
    const T& GetProduct() const;

    // Get the trade ID
    const string& GetTradeId() const;

    // Get the book
    const string& GetBook() const;

    // Get the quantity
    long GetQuantity() const;

    // Get the side
    Side GetSide() const;

private:
    T product;
    string tradeId;
    string book;
    long quantity;
    Side side;

};

template<typename T>
Trade<T>::Trade(const T &_product, string _tradeId, string _book, long _quantity, Side _side) :
product(_product) {
    tradeId = _tradeId;
    book = _book;
    quantity = _quantity;
    side = _side;
}

template<typename T>
const T& Trade<T>::GetProduct() const {
    return product;
}

template<typename T>
const string& Trade<T>::GetTradeId() const {
    return tradeId;
}

template<typename T>
const string& Trade<T>::GetBook() const {
    return book;
}

template<typename T>
long Trade<T>::GetQuantity() const {
    return quantity;
}

template<typename T>
Side Trade<T>::GetSide() const {
    return side;
}

template<typename T>
Position<T>::Position(const T &_product) :
product(_product) {
    for (int i = 0; i < 3; ++i) {
        positions[BOOKs[i]] = 0.;
    }
}

template<typename T>
const T& Position<T>::GetProduct() const {
    return product;
}

template<typename T>
long Position<T>::GetPosition(string &book) {
    return positions[book];
}

// retrieve the total position for this product across all the books
template<typename T>
long Position<T>::GetAggregatePosition() const {
    long res = 0;
    string s = "TRSY";
    for (int i = 0; i < 3; i++) {
        string key = s + to_string(i+1);
        res += positions.at(key);
    }
    return res;
}

template<typename T>
void Position<T>::AddPosition(string& book, int n) {
    assert(positions.find(book) != positions.end());
    positions[book] += n;
}

template<typename T>
void Position<T>::AddPosition(Position<T>& a) {
    for (int i = 0; i < 3; ++i)
        positions[BOOKs[i]] += a.GetPosition(BOOKs[i]);
}

template<typename T>
Position<T>::Position() {

}

template<typename T>
OrderBook<T>::OrderBook(const T &_product, const vector<Order> &_bidStack, const vector<Order> &_offerStack) :
product(_product), bidStack(_bidStack), offerStack(_offerStack) {
}

template<typename T>
const T& OrderBook<T>::GetProduct() const {
    return product;
}

template<typename T>
const vector<Order>& OrderBook<T>::GetBidStack() const {
    return bidStack;
}

template<typename T>
const vector<Order>& OrderBook<T>::GetOfferStack() const {
    return offerStack;
}

Order::Order(double _price, long _quantity, PricingSide _side) {
    price = _price;
    quantity = _quantity;
    side = _side;
}

double Order::GetPrice() const {
    return price;
}

long Order::GetQuantity() const {
    return quantity;
}

PricingSide Order::GetSide() const {
    return side;
}

template<typename T>
class PV01 {
public:

    // ctor for a PV01 value
    PV01(const T &_product, double _pv01, long _quantity);

    // Get the product on this PV01 value

    const T& GetProduct() const {
        return product;
    }

    // Get the PV01 value

    double GetPV01() const {
        return pv01;
    }

    // Get the quantity that this risk value is associated with

    long GetQuantity() const {
        return quantity;
    }

private:
    T product;
    double pv01;
    long quantity;

};

/**
 * Inquiry object modeling a customer inquiry from a client.
 * Type T is the product type.
 */
template<typename T>
class Inquiry {
public:

    // ctor for an inquiry
    Inquiry(string _inquiryId, const T &_product, Side _side, long _quantity, double _price, InquiryState _state);

    // Get the inquiry ID

    const string& GetInquiryId() const {
        return inquiryId;
    }

    // Get the product

    const T& GetProduct() const {
        return product;
    }

    // Get the side on the inquiry

    Side GetSide() const {
        return side;
    }

    // Get the quantity that the client is inquiring for

    long GetQuantity() const {
        return quantity;
    }

    // Get the price that we have responded back with

    double GetPrice() const {
        return price;
    }

    // Get the current state on the inquiry

    InquiryState GetState() const {
        return state;
    }

    void SetState(InquiryState s) {
        state = s;
    }

private:
    string inquiryId;
    T product;
    Side side;
    long quantity;
    double price;
    InquiryState state;

};

/**
 * A price object consisting of mid and bid/offer spread.
 * Type T is the product type.
 */
template<typename T>
class Price {
public:

    // ctor for a price
    Price(const T &_product, double _mid, double _bidOfferSpread);

    // Get the product
    const T& GetProduct() const;

    // Get the mid price
    double GetMid() const;

    // Get the bid/offer spread around the mid
    double GetBidOfferSpread() const;

private:
    const T& product;
    double mid;
    double bidOfferSpread;

};

template<typename T>
Price<T>::Price(const T &_product, double _mid, double _bidOfferSpread) :
product(_product) {
    mid = _mid;
    bidOfferSpread = _bidOfferSpread;
}

template<typename T>
const T& Price<T>::GetProduct() const {
    return product;
}

template<typename T>
double Price<T>::GetMid() const {
    return mid;
}

template<typename T>
double Price<T>::GetBidOfferSpread() const {
    return bidOfferSpread;
}

/**
 * Price Stream with a two-way market.
 * Type T is the product type.
 */

/**
 * A price stream order with price and quantity (visible and hidden)
 */
class PriceStreamOrder {
public:

    // ctor for an order
    PriceStreamOrder(double _price, long _visibleQuantity, long _hiddenQuantity, PricingSide _side);

    // The side on this order
    PricingSide GetSide() const;

    // Get the price on this order
    double GetPrice() const;

    // Get the visible quantity on this order
    long GetVisibleQuantity() const;

    // Get the hidden quantity on this order
    long GetHiddenQuantity() const;

private:
    double price;
    long visibleQuantity;
    long hiddenQuantity;
    PricingSide side;

};

template<typename T>
class PriceStream {
public:

    // ctor
    PriceStream(const T &_product, const PriceStreamOrder &_bidOrder, const PriceStreamOrder &_offerOrder);

    // Get the product
    const T& GetProduct() const;

    // Get the bid order
    const PriceStreamOrder& GetBidOrder() const;

    // Get the offer order
    const PriceStreamOrder& GetOfferOrder() const;

private:
    T product;
    PriceStreamOrder bidOrder;
    PriceStreamOrder offerOrder;

};

template<typename T>
PriceStream<T>::PriceStream(const T &_product, const PriceStreamOrder &_bidOrder, const PriceStreamOrder &_offerOrder) :
product(_product), bidOrder(_bidOrder), offerOrder(_offerOrder) {
}

template<typename T>
const T& PriceStream<T>::GetProduct() const {
    return product;
}

PriceStreamOrder::PriceStreamOrder(double _price, long _visibleQuantity, long _hiddenQuantity, PricingSide _side) {
    price = _price;
    visibleQuantity = _visibleQuantity;
    hiddenQuantity = _hiddenQuantity;
    side = _side;
}

double PriceStreamOrder::GetPrice() const {
    return price;
}

long PriceStreamOrder::GetVisibleQuantity() const {
    return visibleQuantity;
}

long PriceStreamOrder::GetHiddenQuantity() const {
    return hiddenQuantity;
}

template<typename T>
const PriceStreamOrder& PriceStream<T>::GetBidOrder() const {
    return bidOrder;
}

template<typename T>
const PriceStreamOrder& PriceStream<T>::GetOfferOrder() const {
    return offerOrder;
}


std::map<string, Position <Bond>> POSITION;

#endif /* TRADE_HPP */

