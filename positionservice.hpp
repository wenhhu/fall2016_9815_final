/**
 * positionservice.hpp
 * Defines the data types and Service for positions.
 *
 * @author Breman Thuraisingham
 */
#ifndef POSITION_SERVICE_HPP
#define POSITION_SERVICE_HPP

#include <string>
#include <map>
#include "soa.hpp"
#include "common.hpp"
#include "riskservice.hpp"

using namespace std;

/**
 * 
 * Position class in a particular book.
 * Type T is the product type.
 */


/**
 * Position Service to manage positions across multiple books and secruties.
 * Keyed on product identifier.
 * Type T is the product type.
 */

template<typename V>
class PositionServiceListener : public ServiceListener<Position<V>>
{
    RiskService* service = NULL;
public:
    PositionServiceListener();
    void ProcessAdd(Position<V>&);

    void ProcessRemove(const Position<V>&) {
    }

    void ProcessUpdate(const Position<V>&) {
    }
};

template<typename T>
class PositionService : public Service<string, Position<T>>
{
public:
    // Add a trade to the service
    PositionService();
    virtual void AddTrade(Position<T> &trade);
    virtual void OnMessage(Position<T>&);
};

//update the global position variable once any new position shows up
template<typename T>
void PositionService<T>::AddTrade(Position<T>& pos) {
    string id = pos.GetProduct().GetProductId();
    if(POSITION.count(id))
        POSITION[id].AddPosition(pos);
    else{
        POSITION.insert(std::pair<string,Position<Bond>>(id,pos));
    }
}

template<typename T>
void PositionService<T>::OnMessage(Position<T>& data) {
    cout << "Add Trade to position..." << endl;
    AddTrade(data);
    for (auto it = this->Listeners.begin(); it != this->Listeners.end(); ++it) {
        (*it)->ProcessAdd(data);
    }
}

template<typename V>
PositionService<V>::PositionService()
{
    ServiceListener< Position<V> >* init = new PositionServiceListener<V>;
    this->AddListener(init);
}

template<typename V>
PositionServiceListener<V>::PositionServiceListener(){
    service = new RiskService;
}

template<typename V>
void PositionServiceListener<V>::ProcessAdd(Position<V>& data) {
    service->AddPosition(data);
}



#endif
