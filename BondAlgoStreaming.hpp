/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BondAlgoStreaming.hpp
 * Author: wenhaohu
 *
 * Created on December 18, 2016, 11:16 PM
 */

#ifndef BONDALGOSTREAMING_HPP
#define BONDALGOSTREAMING_HPP

#include "common.hpp"
#include "streamingservice.hpp"

class AlgoStreamingService : public Service<string, Price<Bond>> {
public:

    // ctor
    AlgoStreamingService(){AddListener(new StreamingListener);}
    void OnMessage(Price<Bond>&);

private:

};

class BondAlgoStreamingListener : public ServiceListener<Price<Bond>>
{
    AlgoStreamingService* service;
public:
    void ProcessAdd(Price<Bond>&);
    BondAlgoStreamingListener(){service = new AlgoStreamingService;}
};

void BondAlgoStreamingListener::ProcessAdd(Price<Bond>& data) {
    service->OnMessage(data);
}

void AlgoStreamingService::OnMessage(Price<Bond>& data){
    for(auto it = Listeners.begin();it!=Listeners.end();++it)
        (*it)->ProcessAdd(data);
}

#endif /* BONDALGOSTREAMING_HPP */

