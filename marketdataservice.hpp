/**
 * marketdataservice.hpp
 * Defines the data types and Service for order book market data.
 *
 * @author Breman Thuraisingham
 */
#ifndef MARKET_DATA_SERVICE_HPP
#define MARKET_DATA_SERVICE_HPP

#include <string>
#include <vector>
#include "soa.hpp"
#include "common.hpp"
#include "BondAlgoExecution.hpp"

using namespace std;


/**
 * Class representing a bid and offer order
 */
class BidOffer {
public:

    // ctor for bid/offer
    BidOffer(const Order &_bidOrder, const Order &_offerOrder);

    // Get the bid order
    const Order& GetBidOrder() const;

    // Get the offer order
    const Order& GetOfferOrder() const;

private:
    Order bidOrder;
    Order offerOrder;

};



/**
 * Market Data Service which distributes market data
 * Keyed on product identifier.
 * Type T is the product type.
 */

class MarketDataService : public Service<string, OrderBook <Bond> > {
//    map<string, OrderBook<T>> ORDERBOOK;

public:
    
    MarketDataService();

    virtual void OnMessage(OrderBook<Bond> &data);
    
    // Get the best bid/offer order
    virtual const BidOffer& GetBestBidOffer(const string &productId){}

    // Aggregate the order book
    virtual const OrderBook<Bond>& AggregateDepth(const string &productId){}

};

class MarketDataConnector : public Connector<OrderBook<Bond>> {
private:
    MarketDataService* service;
public:
    void FetchData();

    void Publish(OrderBook<Bond> &data) {
    }
    MarketDataConnector(){service = new MarketDataService;}
};

BidOffer::BidOffer(const Order &_bidOrder, const Order &_offerOrder) :
bidOrder(_bidOrder), offerOrder(_offerOrder) {
}

const Order& BidOffer::GetBidOrder() const {
    return bidOrder;
}

const Order& BidOffer::GetOfferOrder() const {
    return offerOrder;
}

void MarketDataConnector::FetchData() {
    ifstream file("marketdata.csv");
    std::string line;
    std::vector<std::string> temp;
    getline(file, line);
    boost::split(temp, line, boost::is_any_of(","));
    auto bondID = (temp[0] == "CUSIP") ? CUSIP : ISIN;
    while (getline(file, line)) {
        boost::split(temp, line, boost::is_any_of(","));
//        cout << temp[0] << endl;
        Bond* b = new Bond(temp[0], bondID, "T", BondInfo[temp[0]].first, BondInfo[temp[0]].second);
        vector<Order> bidstack, offerstack;
        for(int i=0;i<5;++i){
            bidstack.push_back(Order(BondPriceConv(temp[9-2*i]),stof(temp[10-2*i]),BID));
            offerstack.push_back(Order(BondPriceConv(temp[12+2*i]),stof(temp[13+2*i]),OFFER));
        }
        OrderBook<Bond> orderbook(*b, bidstack, offerstack);
        service->OnMessage(orderbook);
    }
    file.close();
}

MarketDataService::MarketDataService(){
    AddListener(new BondAlgoExecutionListener);
}

//inform all the listener registered on MarketDataService
void MarketDataService::OnMessage(OrderBook<Bond> &data){
    cout << "MarketDataService..." << endl;
    for(auto it = Listeners.begin();it!=Listeners.end();++it)
        (*it)->ProcessUpdate(data);
}

#endif
