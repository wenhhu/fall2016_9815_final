/**
 * tradebookingservice.hpp
 * Defines the data types and Service for trade booking.
 *
 * @author Breman Thuraisingham
 */
#ifndef TRADE_BOOKING_SERVICE_HPP
#define TRADE_BOOKING_SERVICE_HPP

#include <string>
#include <vector>
#include "soa.hpp"
#include "positionservice.hpp"
#include "common.hpp"
#include <boost/algorithm/string.hpp>


/**
 * Trade object with a price, side, and quantity on a particular book.
 * Type T is the product type.
 */


template<typename V>
class TradeServiceListener : public ServiceListener<Trade<V>>
{
    PositionService<V>* service;
public:
    void ProcessAdd(Trade<V>&);
    void ProcessRemove(Trade<V> &data){}
    void ProcessUpdate(Trade<V> &data){}
    TradeServiceListener();
};

template<typename V>
class TradeBookingService : public Service<string, Trade<V>> {
public:
    TradeBookingService(){this->AddListener(new TradeServiceListener<V>);}
    void BookTrade(Trade<V> &trade);
    void OnMessage(Trade<V>& data);
};


class BondTradeBookingConnector : public Connector<Trade<Bond>>
{
private:
    TradeBookingService<Bond>* service;
public:
    void FetchData();
    void Publish(Trade<Bond> &data);
    BondTradeBookingConnector();
};
/**
 * Trade Booking Service to book trades to a particular book.
 * Keyed on product identifier.
 * Type T is the product type.
 */

//retrieve data from generated trading data
void BondTradeBookingConnector::FetchData() {
    ifstream file("trade.csv");
    std::string line;
    std::vector<std::string> temp;
    getline(file, line);
    boost::split(temp, line, boost::is_any_of(","));
    auto bondID = (temp[0] == "CUSIP")?CUSIP:ISIN;
    while (getline(file, line)) {
        boost::split(temp, line, boost::is_any_of(","));
        std::vector<std::string> ddd;
        boost::split(ddd, temp[6], boost::is_any_of("/"));
        Bond* b = new Bond(temp[0], bondID, "T", stof(temp[7]), date(stoi(ddd[0]),stoi(ddd[1]),stoi(ddd[2])));
//        cout << *b << endl;
        Trade<Bond> trade(*b, temp[1], temp[2], stof(temp[3]), (temp[4]=="BUY")?BUY:SELL);
        service->OnMessage(trade);
    }
    file.close();
}

void BondTradeBookingConnector::Publish(Trade<Bond>& data) {

}

BondTradeBookingConnector::BondTradeBookingConnector(){
    service = new TradeBookingService<Bond>();
}

template<typename V>
void TradeBookingService<V>::BookTrade(Trade<V>& trade) {
    for (auto it = this->Listeners.begin(); it != this->Listeners.end(); ++it) {
        (*it)->ProcessAdd(trade);
    }
}

template<typename V>
void TradeBookingService<V>::OnMessage(Trade<V>& data){
    cout << "TradeBookingService...\n";
    BookTrade(data);
}

//convert trade to position
template<typename V>
void TradeServiceListener<V>::ProcessAdd(Trade<V>& trade) {
    std::string id = trade.GetProduct().GetProductId();
    std::string book = trade.GetBook();
    int quantity = trade.GetQuantity();
    Position<Bond> pos(trade.GetProduct());
    pos.AddPosition(book, (trade.GetSide()==BUY)?quantity:-quantity);
    service->OnMessage(pos);
}

template<typename V>
TradeServiceListener<V>::TradeServiceListener() {
    service = new PositionService<V>;
}

#endif
