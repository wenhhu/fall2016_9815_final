/**
 * riskservice.hpp
 * Defines the data types and Service for fixed income risk.
 *
 * @author Breman Thuraisingham
 */
#ifndef RISK_SERVICE_HPP
#define RISK_SERVICE_HPP

#include "soa.hpp"
#include "positionservice.hpp"
#include "historicaldataservice.hpp"
//#include "common.hpp"

/**
 * PV01 risk.
 * Type T is the product type.
 */

/**
 * A bucket sector to bucket a group of securities.
 * We can then aggregate bucketed risk to this bucket.
 * Type T is the product type.
 */
template<typename T>
class BucketedSector {
public:

    // ctor for a bucket sector
    BucketedSector(const vector<T> &_products, string _name);

    // Get the products associated with this bucket
    const vector<T>& GetProducts() const;

    // Get the name of the bucket
    const string& GetName() const;

private:
    vector<T> products;
    string name;

};

/**
 * Risk Service to vend out risk for a particular security and across a risk bucketed sector.
 * Keyed on product identifier.
 * Type T is the product type.
 */

class RiskService : public Service<string, PV01 <Bond> > {
    std::map<string, double> single_PV01;
public:

    RiskService();

    virtual void OnMessage(PV01 <Bond>&){}

    // Add a position that the service will risk

    virtual void AddPosition(const Position<Bond> &position);

    // Get the bucketed risk for the bucket sector

    virtual const PV01<Bond>& GetBucketedRisk(const BucketedSector<Bond> &sector) const {
    }

};

class RiskServiceListener : public ServiceListener<PV01 <Bond> > {
    Service<string, Bond>* service = NULL;
public:
    RiskServiceListener(){}
    void ProcessAdd(PV01<Bond>&);

    void ProcessRemove(const PV01<Bond>&) {
    }

    void ProcessUpdate(const PV01<Bond>&) {
    }
};

template<typename T>
PV01<T>::PV01(const T &_product, double _pv01, long _quantity) :
product(_product) {
    pv01 = _pv01;
    quantity = _quantity;
}

template<typename T>
BucketedSector<T>::BucketedSector(const vector<T>& _products, string _name) :
products(_products) {
    name = _name;
}

template<typename T>
const vector<T>& BucketedSector<T>::GetProducts() const {
    return products;
}

template<typename T>
const string& BucketedSector<T>::GetName() const {
    return name;
}

void RiskService::AddPosition(const Position<Bond> & position) {
    //PV01 corresponding to 2, 3, 5, 7, 10, 30 yrs T-bond
    cout << "Adding positon in RiskService.\n";
    string id = position.GetProduct().GetProductId();
    long _quantity = position.GetAggregatePosition();
    double _risk = single_PV01[id] * position.GetAggregatePosition() / 100.0;
    PV01<Bond> pv01(position.GetProduct(), _risk, _quantity);
    HistoricalDataService::get_instance()->PersistRiskData(pv01);
}

RiskService::RiskService() {
    AddListener(new RiskServiceListener);
    //PV01 for a single bond
    single_PV01[CUSIPs[0]] = 0.0200340579;
    single_PV01[CUSIPs[1]] = 0.02505763255;
    single_PV01[CUSIPs[2]] = 0.05002501251;
    single_PV01[CUSIPs[3]] = 0.06542526422;
    single_PV01[CUSIPs[4]] = 0.09401441554;
    single_PV01[CUSIPs[5]] = 0.2007395668;
}

void RiskServiceListener::ProcessAdd(PV01<Bond>&){
    
}

#endif
