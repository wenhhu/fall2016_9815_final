# README #

This code has passed the complilation test with gcc5.2 in the OSX environment. The command to compile this code is:

g++ -std=c++11 main.cpp

Possibly, you need to add include flag if you didn't include your boost in the environmental variable.

### Some comments on this project ###

* In some templates, we modified it to normal class since we only focus on bond in this project
* HistoricalDataService is implemented with singleton paradigm since it's invoked by many services
* All the infrastructure class and global variables are putted in "common.hpp", which make them easier to be referenced
* Implementation and declaration are both in hpp files since this is a almost pure template code
* Because the data files are too large, we only ran the case with 1000 records for demonstration. But the pricing and marketdata with 1000000 records are created as well.