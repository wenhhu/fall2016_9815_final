/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: wenhaohu
 *
 * Created on December 2, 2016, 3:56 PM
 */

#include "common.hpp"
#include <cstdlib>
#include "products.hpp"
#include "fstream"
#include "positionservice.hpp"
#include "TradeBookingService.hpp"
#include "marketdataservice.hpp"
#include "inquiryservice.hpp"
#include "pricingservice.hpp"
#include <string>
#include <random>


using namespace std;

/*
 * 
 */


void generateTradeBook() {
    ofstream tradebook("trade.csv");
    tradebook << "CUSIP," << "Trade ID," << "Books," << "Quantity," << "Side," << "Price," << "Maturity Date," << "Coupon(%)" << endl;

    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 10; ++j) {
            tradebook << CUSIPs[i] << "," << i * 10 + j + 1 << "," << "TRSY" << rand() % 3 + 1 << ",";
            tradebook << (rand() % 9 + 1)*1000000 << ",";
            tradebook << (rand() % 2 == 0 ? "BUY," : "SELL,");
            tradebook << 99 + rand() % 3 << "-";
            int xy = rand() % 32;
            if (xy < 10)
                tradebook << 0 << xy << rand() % 8 << ",";
            else
                tradebook << xy << rand() % 8 << ",";
            tradebook << MaturityDate[i] << "," << Coupon[i] << endl;
        }
    }
    tradebook.close();
}

void generateMarketData() {
    ofstream marketdata("marketdata.csv");
    marketdata << "CUSIP" << "," << "Bid 1," << "Quantity"
            << "," << "Bid 2," << "Quantity"
            << "," << "Bid 3," << "Quantity"
            << "," << "Bid 4," << "Quantity"
            << "," << "Bid 5," << "Quantity"
            << "," << "spread"
            << "," << "Offer 5," << "Quantity"
            << "," << "Offer 4," << "Quantity"
            << "," << "Offer 3," << "Quantity"
            << "," << "Offer 2," << "Quantity"
            << "," << "Offer 1," << "Quantity" << endl;

    for (int i = 0; i < 1e6; ++i) {
        for (int j = 0; j < 6; ++j) {
            int cur = (i * 6 + j) % (256 * 2);
            double top_bid = 99 + cur / 256. - 1 / 256.,
                    bottom_offer = 99 + cur / 256. + 1 / 256.;
            //            cout << bottom_offer << endl;
            marketdata << CUSIPs[j] << ",";
            BondPrice(marketdata, top_bid - 4 / 256., 5e6) << ",";
            BondPrice(marketdata, top_bid - 3 / 256., 4e6) << ",";
            BondPrice(marketdata, top_bid - 2 / 256., 3e6) << ",";
            BondPrice(marketdata, top_bid - 1 / 256., 2e6) << ",";
            BondPrice(marketdata, top_bid, 1e6) << "," << ",";
            BondPrice(marketdata, bottom_offer, 1e6) << ",";
            BondPrice(marketdata, bottom_offer + 1 / 256., 2e6) << ",";
            BondPrice(marketdata, bottom_offer + 2 / 256., 3e6) << ",";
            BondPrice(marketdata, bottom_offer + 3 / 256., 4e6) << ",";
            BondPrice(marketdata, bottom_offer + 4 / 256., 5e6) << ",";
            marketdata << endl;
        }
    }
    marketdata.close();
}

void generateInquiryData() {
    ofstream file("inquiries.csv");
    file << "Inquiry ID," << "CUSIP," << "Side," << "Price," << "Quantity," << "state" << endl;

    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 10; ++j) {
            file << i * 10 + j + 1 << "," << CUSIPs[i] << "," << ((rand() % 2) == 0 ? "BUY," : "SELL,");
            BondPrice(file, 99. + (rand() % 512) / 256., (rand() % 9 + 1)*1000000) << ((rand() % 2) == 0 ? "RECEIVED," : "SELL,") << endl;
        }
    }
    file.close();
}

void generatePricesData() {
    ofstream file("prices.csv");
    file << "CUSIP" << "," << "Bid"
            << "," << "offer" << endl;

    for (int i = 0; i < 1e6; ++i) {
        for (int j = 0; j < 6; ++j) {
            int cur = (i * 6 + j) % (256 * 2);
            double mid_price = 99 + cur / 256. - 1 / 256.;
            double bid = mid_price - (rand() % 4) / 256.;
            double offer = 2 * mid_price - bid;
            file << CUSIPs[j] << ",";
            BondPrice(file, bid) << ",";
            BondPrice(file, offer) << ",";
            file << endl;
        }
    }
    file.close();
}

int main(int argc, char** argv) {
    cout << "***********************************Test*******************************************" << endl;
//    generate testing data
    generateTradeBook();
//    generateMarketData();
    generateInquiryData();
//    generatePricesData();
//    test trade booking service
//    BondTradeBookingConnector test1;
//    test1.FetchData();
//    test marketalgotrading module
    MarketDataConnector test2;
    test2.FetchData();
//    test inquiry module
//    InquiryConnector test3;
//    test3.FetchData();
//    test pricing/streaming module 
//    PricingServiceConnector test4;
//    test4.FetchData();
    return 0;
}

