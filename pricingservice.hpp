/**
 * pricingservice.hpp
 * Defines the data types and Service for internal prices.
 *
 * @author Breman Thuraisingham
 */
#ifndef PRICING_SERVICE_HPP
#define PRICING_SERVICE_HPP

#include <string>
#include "soa.hpp"
#include "BondAlgoStreaming.hpp"



/**
 * Pricing Service managing mid prices and bid/offers.
 * Keyed on product identifier.
 * Type T is the product type.
 */
class PricingService : public Service<string, Price <Bond> > {
public:
    void OnMessage(Price<Bond> &trade);
    PricingService() {
        AddListener(new BondAlgoStreamingListener);
    }
private:
    void AddListener(BondAlgoStreamingListener* data){this->Listeners.push_back(data);}
    
};

class PricingServiceConnector : public Connector<Price<Bond>>
{
    public:
    void FetchData();

    PricingServiceConnector() {
        service = new PricingService;
    }
    void Publish(Price<Bond>& data){}

    private:
    PricingService *service;
};

//retrieve data from generated price file
void PricingServiceConnector::FetchData() {
    ifstream is("prices.csv");
    string line;
    getline(is, line); // skip the header

    while (getline(is, line)) {
        cout << "Got another price data.\n";
        vector<string> elems;
        boost::split(elems, line, boost::is_any_of(","));
        // get product from CUSIP
        Bond* b = new Bond(elems[0], CUSIP, "T", BondInfo[elems[0]].first, BondInfo[elems[0]].second);
        ;
        double mid_price = (BondPriceConv(elems[1])+BondPriceConv(elems[2]))/2;
        double spread = (BondPriceConv(elems[2])-BondPriceConv(elems[1]))/2;
        Price<Bond> price(*b, mid_price, spread);
        service->OnMessage(price);
    };
}

void PricingService::OnMessage(Price<Bond>& data) {
    cout << "Pricing Service...\n";
    for (auto it = Listeners.begin(); it != Listeners.end(); ++it) {
        (*it)->ProcessAdd(data);
    }
}


#endif
