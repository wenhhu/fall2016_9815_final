/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BondAlgoExecution.hpp
 * Author: wenhaohu
 *
 * Created on December 18, 2016, 4:33 PM
 */

#ifndef BONDALGOEXECUTION_HPP
#define BONDALGOEXECUTION_HPP

#include "common.hpp"
#include "executionservice.hpp"

class BondAlgoExecutionService : public Service<string, OrderBook<Bond>>{
    ExecutionService<Bond>* service;
public:
    void OnMessage(OrderBook<Bond>&);
    BondAlgoExecutionService();
};

class BondAlgoExecutionListener : public ServiceListener<OrderBook<Bond>>
{
    BondAlgoExecutionService* service;
public:
    void ProcessUpdate(OrderBook<Bond>& data);
    void ProcessAdd(OrderBook<Bond> &data){}
    BondAlgoExecutionListener();
};


void BondAlgoExecutionListener::ProcessUpdate(OrderBook<Bond>& data){
//    cout << "BondAlgoExecutionListener..." << endl;
    service->OnMessage(data);
}

BondAlgoExecutionListener::BondAlgoExecutionListener(){
    service = new BondAlgoExecutionService;
}

// alternatingly aggress the top of the book between bid and offer in this module
void BondAlgoExecutionService::OnMessage(OrderBook<Bond>& data){
    cout << "BondAlgoExecutionService" << endl;
    static bool bid = false;
    static int id = 0;
    auto b = data.GetProduct();
    // change order side
    bid = !bid;
    ++id;
    OrderType ordertype = MARKET;
    PricingSide side;
    double visibleQuantity, hiddenQuantity, price;
    // set null parent orderid for simplicity
    string parentOrderId = "";
    // not child order for simplicity
    bool isChildOrder = false;
    if(bid){
        side = BID;
        price = data.GetBidStack().front().GetPrice();
        visibleQuantity = data.GetBidStack().front().GetQuantity();
        hiddenQuantity = data.GetBidStack().front().GetQuantity()-visibleQuantity;
    }
    else{
        side = OFFER;
        price = data.GetOfferStack().front().GetPrice();
        visibleQuantity = data.GetOfferStack().front().GetQuantity();
        hiddenQuantity = data.GetOfferStack().front().GetQuantity()-visibleQuantity;
    }
    ExecutionOrder<Bond> order(data.GetProduct(),side,to_string(id),ordertype,price,visibleQuantity,hiddenQuantity,parentOrderId,isChildOrder);
//    cout << order.GetVisibleQuantity() << endl;
    service->ExecuteOrder(order, BROKERTEC);        
}

BondAlgoExecutionService::BondAlgoExecutionService(){
    service = new ExecutionService<Bond>;
}

#endif /* BONDALGOEXECUTION_HPP */

