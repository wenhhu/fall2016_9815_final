/**
 * inquiryservice.hpp
 * Defines the data types and Service for customer inquiries.
 *
 * @author Breman Thuraisingham
 */
#ifndef INQUIRY_SERVICE_HPP
#define INQUIRY_SERVICE_HPP

#include "soa.hpp"
#include "tradebookingservice.hpp"

/**
 * Service for customer inquirry objects.
 * Keyed on inquiry identifier (NOTE: this is NOT a product identifier since each inquiry must be unique).
 * Type T is the product type.
 */
template<typename T>
class InquiryService : public Service<string, Inquiry <T> > {
public:

    // Send a quote back to the client

    void SendQuote(const string &inquiryId, double price) {
    }

    // Reject an inquiry from the client

    void RejectInquiry(const string &inquiryId) {
    }

    void OnMessage(Inquiry<Bond> &data) {
        cout << "InquiryService..." << endl;
        data.SetState(DONE);
        HistoricalDataService::get_instance()->PersistInquiryData(data);
    }

};

class InquiryConnector : public Connector<Inquiry<Bond>>
{
    InquiryService<Bond>* service;
    public:
    void FetchData();
    InquiryConnector(){service = new InquiryService<Bond>;}
    void Publish(Inquiry<Bond>&) {
    }
};

//for simplicity, all the inquiry is set to DONE in one to-and-fro
void InquiryConnector::FetchData() {
    ifstream file("inquiries.csv");
    std::string line;
    std::vector<std::string> temp;
    getline(file, line);
    boost::split(temp, line, boost::is_any_of(","));
    auto bondID = (temp[0] == "CUSIP") ? CUSIP : ISIN;
    while (getline(file, line)) {
        boost::split(temp, line, boost::is_any_of(","));
        Bond* b = new Bond(temp[1], bondID, "T", BondInfo[temp[1]].first, BondInfo[temp[1]].second);
        //        cout << *b << endl;
        Side side = (temp[2] == "BUY" ? BUY : SELL);
        Inquiry<Bond> inq(temp[0], *b, side, BondPriceConv(temp[3]), stof(temp[4]), RECEIVED);
        service->OnMessage(inq);
    }
    file.close();
}

template<typename T>
Inquiry<T>::Inquiry(string _inquiryId, const T &_product, Side _side, long _quantity, double _price, InquiryState _state) :
product(_product) {
    inquiryId = _inquiryId;
    side = _side;
    quantity = _quantity;
    price = _price;
    state = _state;
}


#endif
